FROM golang:1.21.3-alpine AS build

WORKDIR /app
COPY . .
RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -o ./backend

FROM alpine:3.18.4

COPY --from=build /app/backend /usr/local/bin/backend
EXPOSE 8080

CMD ["/usr/local/bin/backend"]